const
    http = require('http'),
    socketIO = require('socket.io'),
    port = 5000;

const server = http.createServer();
server.listen(port);
io = socketIO( server );

io.on('connection', ( socket ) => {

    console.log(`User ${socket.id} is connected`);

    socket.on('disconnect', () => {

        console.log(`User ${socket.id} is disconnected`);

    });

});